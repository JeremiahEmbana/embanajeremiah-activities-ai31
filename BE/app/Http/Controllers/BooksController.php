<?php

namespace App\Http\Controllers;

use App\Models\Books;
use App\Http\Requests\BooksRequest;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    /**
     * DISPLAY
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $books = Books::all();
        return response()->json([
            "message" => "List of Books",
            "data" => $books]);
    }


    /**
     * ADD
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BooksRequest $request)
    {
        $books = new Books();

        $books->name = $request->name;
        $books->author = $request->author;
        $books->copies = $request->copies;
        $books->category_id = $request->category_id;

        $books->save($request->validated());
        return response()->json($books);
    }


    /**
     * SEARCH
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $books = Books::find($id);
        return response()->json($books);
    }


    /**
     * UPDATE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BooksRequest $request, $id)
    {
        $books = Books::find($id);
        
        $books->name = $request->name;
        $books->author = $request->author;
        $books->copies = $request->copies;
        $books->category_id = $request->category_id;

        $books->update($request->validated());
        return response()->json($books);
    }

    
    /**
     * DELETE
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $books = Books::find($id);
        $books->delete();
        return response()->json($books);
    }
}

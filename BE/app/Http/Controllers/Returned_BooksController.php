<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Returned_BooksRequest;
use App\Models\Returned_Books;


class Returned_BooksController extends Controller
{
    /**
     * DISPLAY
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $returnedbooks = Returned_Books::all();
        return response()->json(["message" => "List of Returned Books",
        "data" => $returnedbooks]);
        //
    }


    /**
     * SEARCH
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $returnedbooks = Returned_Books::find($id);
        return response()->json($returnedbooks);
        //
    }

}

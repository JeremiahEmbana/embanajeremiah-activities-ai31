<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    
    use HasFactory;
    protected $table = 'books';
    protected $fillable = ['name', 'author', 'copies', 'category_id'];

    public function category()
    {
        return $this->belongsTo(Categories::class);
    }


    public function borrowedBooks()
    {
        return $this->hasMany(Borrowed_Books::class, 'borrowed_books', 'book_id');
    }

    
    public function returnedBooks()
    {
        return $this->hasMany(Returned_Books::class, 'returned_books', 'book_id');
    }

}